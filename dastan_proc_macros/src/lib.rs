// #![feature(proc_macro_quote)]

use proc_macro::TokenStream;
use syn::DeriveInput;

mod attrs;

// mod derive_generator_bundle;
mod derive_raw_representation;

// #[proc_macro_derive(SuperGenerator, attributes(generate))]
// pub fn derive_generator_bundle(input: TokenStream) -> TokenStream {
// 	let ast: DeriveInput = syn::parse(input).unwrap();

// 	derive_generator_bundle::derive(&ast)
// }

#[proc_macro_derive(RawRepresentation, attributes(raw))]
pub fn derive_raw_representation(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    derive_raw_representation::derive(&ast).unwrap().into()
}
