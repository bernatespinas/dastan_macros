use proc_macro2::TokenTree;
use syn::{
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    Attribute, Ident, Result, Token,
};

pub fn parse_attrs<A: Parse>(attrs: &[Attribute]) -> Result<Vec<A>> {
    let attrs = attrs
        .iter()
        .filter(|a| a.path.is_ident("raw"))
        .map(|a| a.parse_args_with(Punctuated::<A, Token![,]>::parse_terminated))
        .collect::<Result<Vec<_>>>()?
        .into_iter()
        .flatten()
        .collect();
    Ok(attrs)
}

/// implements `syn::parse::Parse` for the given type
macro_rules! impl_parse {
    // entry point
    ($i:ident {
        $( $s:literal => $v:ident( $($t:tt)* ) ),*
    }) => {
        impl syn::parse::Parse for $i {
            #[allow(clippy::redundant_closure_call)]
            fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
                let ident = input.parse::<syn::Ident>()?;
                match &*ident.to_string() {
                    $( $s => (impl_parse!($($t)*))(input).map(Self::$v), )*
                    _ => Err(input.error("unknown attribute"))
                }
            }
        }
    };
    () => ( |_: ParseStream| Ok(()) );
    // parse either "= {value}" or return None
    ((= $t:tt)?) => {
        |i: ParseStream| if i.peek(syn::Token![=]) {
            i.parse::<syn::Token![=]>()?;
            #[allow(clippy::redundant_closure_call)]
            (impl_parse!($t))(i).map(Some)
        } else {
            Ok(None)
        }
    };
    // parse "= {value}"
    (= $x:tt) => ( |i: ParseStream| {
        i.parse::<syn::Token![=]>()?;
        #[allow(clippy::redundant_closure_call)]
        (impl_parse!($x))(i)
    } );
    (String) => ( |i: ParseStream| i.parse().map(|s: syn::LitStr| s.value()) );
    (bool) => ( |i: ParseStream| i.parse().map(|s: syn::LitBool| s.value()) );
    ($t:ty) => ( |i: ParseStream| i.parse::<$t>() );
}

pub struct AnyAttribute(pub Vec<Attribute>);
impl syn::parse::Parse for AnyAttribute {
    fn parse(input: ParseStream) -> Result<Self> {
        input.call(Attribute::parse_outer).map(Self)
    }
}

pub enum RawFieldAttr {
    Skip(()),
    UseRawForm(()),
    Default(Ident),
}

impl_parse!(RawFieldAttr {
    "skip" => Skip(),
    "use_raw_form" => UseRawForm(),
    "default" => Default(= Ident)
});

pub enum RawRepresentationAttr {
    AuxiliaryGenerator(TokenTree),
}

impl_parse!(RawRepresentationAttr {
    "auxiliary_generator" => AuxiliaryGenerator(= TokenTree)
});
