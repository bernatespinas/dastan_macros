// use proc_macro::{TokenStream};
use proc_macro2::{Ident, Span, TokenStream, TokenTree};
use quote::{quote, ToTokens};
use syn::{DeriveInput, Result, Type};

use crate::attrs::{parse_attrs, RawFieldAttr, RawRepresentationAttr};

pub struct RawRepresentation {
	ident: Ident,
	fields: Vec<RawField>,
	actual_representation: Ident,
	auxiliary_generator: TokenTree,
}

impl TryFrom<&DeriveInput> for RawRepresentation {
	type Error = syn::Error;

	fn try_from(value: &DeriveInput) -> syn::Result<Self> {
		let data = match &value.data {
			syn::Data::Struct(s) => s,
			_ => panic!("not a struct with named fields"),
		};

		let mut auxiliary_generator = None;
		for attr in parse_attrs::<RawRepresentationAttr>(&value.attrs)? {
			match attr {
				RawRepresentationAttr::AuxiliaryGenerator(aux) => auxiliary_generator.replace(aux),
			};
		}

		let fields = data
			.fields
			.iter()
			.map(RawField::try_from)
			.collect::<Result<Vec<_>>>()?;

		let actual_representation = value.ident.clone();
		let ident = Ident::new(&format!("Raw{}", &value.ident), Span::call_site());

		Ok(RawRepresentation {
			ident,
			fields,
			actual_representation,
			auxiliary_generator: auxiliary_generator.unwrap(),
		})
	}
}

pub struct RawField {
	field: Ident,
	ty: Type,
	skip: bool,
	use_raw_form: bool,
	default: Option<Ident>,
}

impl RawField {
	fn ty(&self) -> TokenStream {
		let ty = &self.ty;

		if self.use_raw_form {
			Ident::new(&format!("Raw{}", ty.to_token_stream()), Span::call_site())
				.into_token_stream()
		} else {
			quote! { #ty }
		}
	}
}

impl TryFrom<&syn::Field> for RawField {
	type Error = syn::Error;

	fn try_from(value: &syn::Field) -> syn::Result<Self> {
		let mut skip = false;
		let mut use_raw_form = false;
		let mut default = None;

		for attr in parse_attrs::<RawFieldAttr>(&value.attrs)? {
			match attr {
				RawFieldAttr::Skip(()) => skip = true,
				RawFieldAttr::UseRawForm(()) => use_raw_form = true,
				RawFieldAttr::Default(d) => default = Some(d),
			};
		}

		let ty = value.ty.clone();
		// let ty = if use_raw_form {
		// 	let ident_token_stream = Ident::new(&format!("Raw{}", &value.ty.to_token_stream()), Span::call_site()).into_token_stream();

		// 	Type::Verbatim(ident_token_stream)
		// } else {
		// 	value.ty.clone()
		// };

		Ok(RawField {
			field: (value.ident).clone().unwrap(),
			ty,
			skip,
			use_raw_form,
			default,
		})
	}
}

pub fn derive(input: &DeriveInput) -> Result<TokenStream> {
	let raw_representation: RawRepresentation = RawRepresentation::try_from(input)?;

	let raw_struct = define_raw_struct(&raw_representation)?;
	let impl_from_raw_into_actual = impl_from_raw_into_actual(&raw_representation)?;

	Ok(quote!(
		#raw_struct

		#impl_from_raw_into_actual
	))
}

fn define_raw_struct(raw_representation: &RawRepresentation) -> syn::Result<TokenStream> {
	let ident = &raw_representation.ident;
	let fields: proc_macro2::TokenStream = raw_representation
		.fields
		.iter()
		.filter(|raw_field| !raw_field.skip)
		.map(|raw_field| {
			let raw_field_ident = &raw_field.field;
			let raw_field_type = raw_field.ty();

			quote!(
				#raw_field_ident: #raw_field_type,
			)
		})
		.collect();

	Ok(quote! {
		#[derive(Deserialize)]
		pub struct #ident {
			#fields
		}
	})
}

fn impl_from_raw_into_actual(raw_representation: &RawRepresentation) -> syn::Result<TokenStream> {
	let ident = &raw_representation.ident;
	let auxiliary_generator = &raw_representation.auxiliary_generator;
	// let actual_struct_type = quote!(String);
	let actual_representation = &raw_representation.actual_representation;

	let set_default_fields = set_default_fields(raw_representation);
	let set_unmodified_fields = set_unmodified_fields(raw_representation);
	let set_modified_fields = set_modified_fields(raw_representation);

	Ok(quote! {
		impl FromRawToActual for #ident {
			type AuxiliaryGenerator = #auxiliary_generator;
			type ActualStruct = #actual_representation;

			fn into(self, auxiliary_generator: &Self::AuxiliaryGenerator) -> Self::ActualStruct {
				#actual_representation {
					#set_default_fields
					#set_unmodified_fields
					#set_modified_fields
				}
			}
		}
	})
}

fn set_default_fields(raw_representation: &RawRepresentation) -> TokenStream {
	raw_representation
		.fields
		.iter()
		.filter(|raw_field| raw_field.default.is_some())
		.map(|raw_field| {
			let ident = &raw_field.field;
			let default = raw_field.default.as_ref().unwrap();

			quote! { #ident: #default, }
		})
		.collect()
}

fn set_unmodified_fields(raw_representation: &RawRepresentation) -> TokenStream {
	raw_representation
		.fields
		.iter()
		.filter(|raw_field| raw_field.default.is_none() && !raw_field.use_raw_form)
		.map(|raw_field| {
			let ident = &raw_field.field;

			quote! { #ident: self.#ident, }
		})
		.collect()
}

fn set_modified_fields(raw_representation: &RawRepresentation) -> TokenStream {
	raw_representation
		.fields
		.iter()
		.filter(|raw_field| raw_field.default.is_none() && raw_field.use_raw_form)
		.map(|raw_field| {
			let ident = &raw_field.field;

			quote! { #ident: FromRawToActual::into(self.#ident, auxiliary_generator), }
		})
		.collect()
}
