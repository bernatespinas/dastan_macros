// use syn::{DataStruct, Data, Fields, Attribute, punctuated::Punctuated, Token, DeriveInput, parse::{Parse, ParseStream}};
use proc_macro::TokenStream;
use quote::quote;
use syn::{
	parse::ParseStream,
	Data,
	DataStruct,
	DeriveInput,
	Fields,
	// punctuated::Punctuated,
	// Attribute, , Result, Token,
	Ident,
};

type FieldsComma = syn::punctuated::Punctuated<syn::Field, syn::token::Comma>;

enum GenerateAttr {
	InputThing(Ident),
	OutputThing(Ident),
}

/// implements `syn::parse::Parse` for the given type
macro_rules! impl_parse {
    // entry point
    ($i:ident {
        $( $s:literal => $v:ident( $($t:tt)* ) ),*
    }) => {
        impl syn::parse::Parse for $i {
            #[allow(clippy::redundant_closure_call)]
            fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
                let ident = input.parse::<syn::Ident>()?;
                match &*ident.to_string() {
                    $( $s => (impl_parse!($($t)*))(input).map(Self::$v), )*
                    _ => Err(input.error("unknown attribute"))
                }
            }
        }
    };
    () => ( |_: ParseStream| Ok(()) );
    // parse either "= {value}" or return None
    ((= $t:tt)?) => {
        |i: ParseStream| if i.peek(syn::Token![=]) {
            i.parse::<syn::Token![=]>()?;
            #[allow(clippy::redundant_closure_call)]
            (impl_parse!($t))(i).map(Some)
        } else {
            Ok(None)
        }
    };
    // parse "= {value}"
    (= $x:tt) => ( |i: ParseStream| {
        i.parse::<syn::Token![=]>()?;
        #[allow(clippy::redundant_closure_call)]
        (impl_parse!($x))(i)
    } );
    (String) => ( |i: ParseStream| i.parse().map(|s: syn::LitStr| s.value()) );
    (bool) => ( |i: ParseStream| i.parse().map(|s: syn::LitBool| s.value()) );
    ($t:ty) => ( |i: ParseStream| i.parse::<$t>() );
}

impl_parse!(GenerateAttr {
	"input_thing" => InputThing(= Ident),
	"output_thing" => OutputThing(= Ident)
});

// fn parse_attrs(attrs: &[Attribute]) -> syn::Result<Vec<GenerateAttr>> {
// 	let attrs = attrs
// 		.iter()
// 		.filter(|a| a.path.is_ident("generate"))
// 		.map(|a| a.parse_args_with(Punctuated::<GenerateAttr, Token!(,)>::parse_terminated))
// 		.collect::<Result<Vec<_>>>()?
// 		.into_iter()
// 		.flatten()
// 		.collect();

// 	Ok(attrs)
// }

pub fn derive(input: &DeriveInput) -> TokenStream {
	let fields = match &input.data {
		Data::Struct(DataStruct {
			fields: Fields::Named(named_fields),
			..
		}) => &named_fields.named,
		_ => panic!("TODO I can only work with named fields!"),
	};

	// let attrs = parse_attrs::<GenerateAttr>(&input.attrs).unwrap();

	impl_generator_for_each_attribute(&input.ident, fields).into()
}

fn impl_generator_for_each_attribute(
	ident: &Ident,
	fields: &FieldsComma,
) -> proc_macro2::TokenStream {
	fields
		.into_iter()
		.filter(|field| {
			field
				.attrs
				.iter()
				.find(|attr| attr.path.is_ident("generate"))
				.is_some()
		})
		.map(|field| {
			let field_name = field.ident.as_ref().expect("missing ident in field");
			let generate = &field.attrs.first().unwrap().tokens;

			let field_is_optional = field_name.to_string().starts_with("Option<");

			let field_access = if field_is_optional {
				quote! { #field_name.unwrap() }
			} else {
				quote! { #field_name }
			};

			quote! {
				#[automatically_derived]
				impl SuperGenerator<#generate> for #ident {
					fn generate(&self, key: &str) -> #generate {
						self.#field_access.generate(key)
					}
				}
			}
		})
		.collect()
}
