#[macro_export]
macro_rules! create_generator {
	(
		Generator $generator:ident loads $generates:ident from $dir_with_ron_files:literal
	) => {
		#[derive(Default)]
		pub struct $generator {
			hashmap: HashMap<String, $generates>,
		}

		impl Generator<$generates> for $generator {
			type AuxiliaryGenerator = ();

			fn load(auxiliary_generator: &()) -> Result<Self, String> {
				let hashmap: HashMap<String, $generates> =
					deserialize_ron_definitions($dir_with_ron_files)?;

				Ok(Self { hashmap })
			}

			fn get(&self, key: &str) -> &$generates {
				self.hashmap.get(key).expect(&format!(
					"Unknown key `{}` in Generator {}. Available keys: {:#?}",
					key,
					stringify!($generator),
					self.list()
				))
			}

			fn generate(&self, key: &str) -> $generates {
				self.get(key).clone()
			}

			fn can_generate(&self, key: &str) -> bool {
				self.hashmap.contains_key(key)
			}

			fn list(&self) -> Vec<&String> {
				let mut vec: Vec<&String> = self.hashmap.keys().collect();
				vec.sort();

				vec
			}
		}
	};
	(
		Generator $generator:ident loads $raw_struct:ident from $dir_with_ron_files:literal and turns them into $generates:ident with the help of $aux_generator:tt
	) => {
		#[derive(Default)]
		pub struct $generator {
			hashmap: HashMap<String, $generates>,
		}

		impl Generator<$generates> for $generator {
			type AuxiliaryGenerator = $aux_generator;

			fn load(auxiliary_generator: &$aux_generator) -> Result<Self, String> {
				let raw_hashmap: HashMap<String, $raw_struct> =
					deserialize_ron_definitions($dir_with_ron_files)?;

				let hashmap = raw_hashmap
					.into_iter()
					.map(|(key, raw)| (key, FromRawToActual::into(raw, auxiliary_generator)))
					.collect();

				Ok(Self { hashmap })
			}

			fn generate(&self, key: &str) -> $generates {
				self.hashmap
					.get(key)
					.cloned()
					.expect(&format!(
						"Unknown key `{}` in Generator {}. Available keys: {:#?}",
						key,
						stringify!($generator),
						self.list()
					))
					.generate()
			}

			fn can_generate(&self, key: &str) -> bool {
				self.hashmap.contains_key(key)
			}

			fn list(&self) -> Vec<&String> {
				let mut vec: Vec<&String> = self.hashmap.keys().collect();
				vec.sort();

				vec
			}
		}
	};
}

// #[macro_export]
// macro_rules! create_aux_generator {
// 	(
// 		Auxiliar Generator $aux_generator:ident has ($($attribute_name:literal : $attribute_struct:ident)+)
// 	) => {
// 		struct $aux_generator {
// 			$(
// 				$attribute_name: $attribute_struct
// 			)+
// 		}
// 	};
// }
