#[macro_export]
macro_rules! create_entity_components_structs {
	(
		$($attribute_name:ident -> $component:ident,)+
	) => {
		use crate::ecs::prelude::*;

		#[derive(Debug, Clone, Serialize, Deserialize)]
		pub struct EntityComponents {
			$(
				pub $attribute_name: Option<$component>,
			)+
		}

		impl EntityComponents {
			pub fn mask(&self) -> EntityComponentsMask {
				EntityComponentsMask {
					$(
						$attribute_name: self.$attribute_name.is_some(),
					)+
				}
			}
		}

		pub struct EntityComponentsRef<'a> {
			$(
				pub $attribute_name: Option<&'a $component>,
			)+
		}

		impl<'a> EntityComponentsRef<'a> {
			pub fn mask(&self) -> EntityComponentsMask {
				EntityComponentsMask {
					$(
						$attribute_name: self.$attribute_name.is_some(),
					)+
				}
			}
		}

		dastan_macro_rules::create_zone_components!(
			$(
				$attribute_name -> $component,
			)+
		);

		dastan_macro_rules::create_entity_components_mask!(
			$(
				$attribute_name -> $component,
			)+
		);

		dastan_macro_rules::create_specialized_entity_tracker!(
			$(
				$attribute_name -> $component,
			)+
		);
	};
}

#[macro_export]
macro_rules! create_zone_components {
    (
    	$($attribute_name:ident -> $component:ident,)+
    ) => {
        #[derive(Default, Serialize, Deserialize)]
		pub struct ZoneComponents {
			$(
				pub $attribute_name: HashMap<EntityId, $component>,
			)+
		}

		$(
			impl ZoneComponentManager<$component> for ZoneComponents {
				fn component(&self, entity_id: &EntityId) -> Option<&$component> {
					self.$attribute_name.get(entity_id)
				}

				fn component_mut(&mut self, entity_id: &EntityId) -> Option<&mut $component> {
					self.$attribute_name.get_mut(entity_id)
				}

				fn add_component(&mut self, entity_id: EntityId, component: $component) {
				    self.$attribute_name.insert(entity_id, component);
				}

				fn remove_component(&mut self, entity_id: &EntityId) -> Option<$component> {
				    self.$attribute_name.remove(entity_id)
				}
			}
		)+

		impl ZoneComponents {
			pub fn import_entity(&mut self, entity: &EntityId, components: EntityComponents) {
				$(
					if let Some(component) = components.$attribute_name {
						self.$attribute_name.insert(entity.clone(), component);
					}
				)+
			}

			pub fn export_entity(&mut self, entity: &EntityId) -> EntityComponents {
				EntityComponents {
					$(
						$attribute_name: self.remove_component(entity),
					)+
				}
			}

			pub fn entity_components_ref<'a>(&'a self, entity: &EntityId) -> EntityComponentsRef<'a> {
				EntityComponentsRef {
					$(
						$attribute_name: self.$attribute_name.get(entity),
					)+
				}
			}
		}
    };
}

#[macro_export]
macro_rules! create_entity_components_mask {
	(
		$($attribute_name:ident -> $component:ident,)+
	) => {
		pub struct EntityComponentsMask {
			$(
				pub $attribute_name: bool,
			)+
		}

		impl EntityComponentsMask {
			pub fn has_any_component(&self) -> bool {
				$(
					if self.$attribute_name {
						return true;
					}
				)+

				false
			}
		}
	};
}
#[macro_export]
macro_rules! create_specialized_entity_tracker {
	(
		$($attribute_name:ident -> $component:ident,)+
	) => {
#[derive(Default, Serialize, Deserialize)]
		pub struct SpecializedEntityTracker {
			$(
				pub $attribute_name: crate::map::EntityZonePosTracker,
			)+
			pub no_components: crate::map::EntityZonePosTracker,
		}

		impl SpecializedEntityTracker {
			pub fn get(&self, entity_id: &EntityId) -> Option<&crate::map::ZonePosition> {
				$(
					if self.$attribute_name.get(entity_id).is_some() {
						return self.$attribute_name.get(entity_id);
					}
				)+

				self.no_components.get(entity_id)
			}

			pub fn set(&mut self, entity_id: EntityId, components_mask: &EntityComponentsMask, zone_pos: crate::map::ZonePosition) {
				if !components_mask.has_any_component() {
					self.no_components.set(entity_id, zone_pos);

					return;
				}

				$(
					if components_mask.$attribute_name {
						self.$attribute_name.set(entity_id, zone_pos);
					}
				)+
			}

			pub fn update_components(&mut self, entity_id: EntityId, components_mask: &EntityComponentsMask) {
				let zone_pos = *self.get(&entity_id).unwrap();

				$(
					if components_mask.$attribute_name {
						self.$attribute_name.set(entity_id, zone_pos);
					} else {
						self.$attribute_name.remove(&entity_id);
					}
				)+

				if components_mask.has_any_component() {
					self.no_components.remove(&entity_id);
				} else {
					self.no_components.set(entity_id, zone_pos);
				}
			}

			pub fn remove(&mut self, entity_id: &EntityId) {
				$(
					self.$attribute_name.remove(entity_id);
				)+

				self.no_components.remove(entity_id);
			}

			pub fn import(&mut self, other: Self) {
				$(
					for (entity_id, zone_pos) in other.$attribute_name.iter() {
						self.$attribute_name.set(*entity_id, *zone_pos);
					}
				)+

				for (entity_id, zone_pos) in other.no_components.iter() {
					self.no_components.set(*entity_id, *zone_pos);
				}
			}
		}
	};
}
